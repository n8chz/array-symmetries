# see https://en.wikipedia.org/wiki/Examples_of_groups#The_symmetry_group_of_a_square:_dihedral_group_of_order_8

class Array # yeah it's a monkey patch
  # Returns `true` if the array is square, which is to say, the length of each
  # row is equal to the number of rows.  Otherwise returns `false`.
  def square?
    all? {|e| e.length == length}
  end


  # Returns the array obtained by flipping the square array vertically.
  def b
    raise ArgumentError unless square?
    reverse
  end

  # Returns the array obtained by rotating the square 90&deg; clockwise.
  def a
    raise ArgumentError unless square?
    b.transpose
  end

  # Returns an array containing the eight symmetries of the square array.
  def symmetries
    raise ArgumentError unless square?
    [self, b, a, a.a, a.a.a, a.b, a.a.b, a.a.a.b]
  end

  # Returns a 2x2 array consisting of the four corners of the original square.
  def corners
    raise ArgumentError unless square?
    [[first.first, first.last], [last.first, last.last]]
  end

  def self.coord_square(n)
    return ArgumentError unless n.is_a?(Integer) && n > 0
    Array.new(n) do |i|
      Array.new(n) do |j|
        [i, j]
      end
    end
  end

  def self.patterns(n)
    coord_square(n).symmetries
  end
end

