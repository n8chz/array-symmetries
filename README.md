Monkeypatch of Ruby's `Array` class which provides the following methods:

## #square?

Returns `true` if the array is square, which is to say, the length of each row is equal to the number of rows.  Otherwise returns `false`.

## #b

Returns the array obtained by flipping the square array vertically.

## #a

Returns the array obtained by rotating the square 90&deg; clockwise.

## #symmetries

Returns an array containing the eight symmetries of the square array.

## #corners

Returns a 2x2 array consisting of the four corners of the original square.



